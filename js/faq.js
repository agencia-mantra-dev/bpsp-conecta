(function ($) {
    $.fn.showHide = function (options) {
 
        //veriaveis default
        var defaults = {
            speed: 500,
            easing: '',
            changeText: 0,
            showText: 'Show',
            hideText: 'Hide' 
        };
        
        var options = $.extend(defaults, options);
 
        $(this).click(function () {
            // optionally add the class .toggleDiv to each div you want to automatically close
            $('.toggleDiv').slideUp('500');

            if ($(this).hasClass('ativo')) {
                $('.bt_click').removeClass('ativo');
                return;
            }

            if (!$(this).hasClass('ativo')) {
                // this var stores which button you've clicked
                var toggleClick = $(this);
                $('.bt_click').removeClass('ativo');

                $(toggleClick).addClass('ativo');
                // this reads the rel attribute of the button to determine which div id to toggle
                var toggleDiv = $(this).attr('rel');
                // here we toggle show/hide the correct div at the right speed and using which easing effect
                $(toggleDiv).slideToggle('500', function() {
                    // this only fires once the animation is completed
                    if(options.changeText==1){
                        //$(toggleDiv).is(":visible") ? toggleClick.text(options.hideText) : toggleClick.text();
                    }
                });
            }
            
            return false;
        });
 
    };
})(jQuery);

$(document).ready(function(){
   $('.bt_click').showHide(); 
});