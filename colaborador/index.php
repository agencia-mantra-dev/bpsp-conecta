<!DOCTYPE HTML>
<html lang="pt-BR">

    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Conecta | Beneficência Portuguesa de São Paulo</title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="Marina Tieko Takahasi | Agência Mantra" />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" href="../css/swag.css" />
        <link rel="stylesheet" href="../css/home.css" />
        <link rel="stylesheet" href="../css/jquery-ui.css" />
        <link rel="stylesheet" href="../css/mobile.css" />
        <link rel="stylesheet" href="../css/font-awesome.css" />

        <!--[if lt IE 9]><script src="../http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <link href="../owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="../owl-carousel/owl.theme.css" rel="stylesheet">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

        <script src="../js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="../css/jquery.modal.css" type="text/css" media="screen" />

    </head>

    <body>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-34928349-7', 'auto');
    ga('send', 'pageview');

    </script>
        
        <header id="header">
            <section class="line height04"></section>
            <section class="central">
                <div class="row">
                    <div class="col3">
                        <h1 class="logo-conecta"><img src="../images/logo-conecta.png" width="115" alt="Conecta" title="Conecta"></h1>
                    </div>
                    <div class="col6 titulo-topo">
                        <h2>AGENDE SEU TREINAMENTO NO TASY</h2>
                    </div>
                    <div class="col3 last logo-bp">
                        <img src="../images/logo-bp.png" width="151" alt="Beneficência Portuguesa de São Paulo" title="Beneficência Portuguesa de São Paulo">
                    </div>
                </div>
            </section>
            <section class="line height01"></section>
        </header>

        <section id="banner">
            <section class="central">
                <h3>O que é o Philips Tasy?</h3>
                <p>O Philips Tasy é um sistema de gestão hospitalar que proporcionará mais segurança<br>aos pacientes, melhoria da eficiência operacional e diversos outros benefícios à instituição<br>e seus médicos, pacientes e colaboradores.</p>
                <a id="irabas" href="javascript:;">Saiba mais</a>
            </section>
        </section>

        <!-- <section id="agendamento">
            <section class="central">
                <div class="col12 steps">
                    <h4 class="data"><span class="active">1</span> data</h4>
                    <h4 class="horario"><span>2</span> horário</h4>
                    <h4 class="dados"><span>3</span> dados</h4>
                </div>
                <div class="col4 col-active">
                    <div id="datepicker"></div>
                </div>
                <div class="col4 horarios">
                    <dl>
                        <dt>23 de agosto</dt>
                        <dd>
                            <ul>
                                <li class="disable">09:00</li>
                                <li class="disable">10:00</li>
                                <li class="disable">11:00</li>
                                <li class="disable">12:00</li>
                                <li class="disable">13:00</li>
                                <li class="disable">14:00</li>
                                <li class="disable">15:00</li>
                                <li class="disable">16:00</li>
                                <li class="disable">17:00</li>
                                <li class="disable">18:00</li>
                                <li class="disable">19:00</li>
                                <li class="disable">20:00</li>
                            </ul>
                        </dd>
                    </dl>
                </div>
                <div class="col4 last dados">
                    <fieldset>
                        <div ng-app>
                            <input type="text" ng-model="nome" placeholder="Nome">
                            <input type="text" ng-model="crm" placeholder="CRM">
                            <input type="text" placeholder="E-mail">
                            <div class="termos">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vehicula lobortis hendrerit. Quisque nulla enim, mollis sed ornare a, facilisis tempor arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam rutrum mi et lorem auctor vestibulum. Etiam pretium vulputate maximus. Morbi mauris nibh, finibus at luctus et, eleifend sit amet eros. Donec suscipit molestie libero vitae condimentum. </div>
                            <p class="text">Eu "<span>{{ nome }}</span>" portador do CRM "<span>{{ crm }}</span>" lorem ipsum dolor sit amet.</p>
                            
                            <div class="squaredOne">
                                <input type="checkbox" value="None" id="squaredOne" name="check" />
                                <label for="squaredOne"></label>
                            </div>
                            <label for="aceito">Li e aceito</label>

                            <button>Enviar</button>
                        </div>
                    </fieldset>
                </div>
            </section>
        </section> -->

        <section id="abas">
            <section class="central">
                <div class="content">
                    <ul class="tabs" style="width:101%;">
                        <li><a href="#tab1" class="active">O que é?</a></li>
                        <li><a href="#tab2">Como?</a></li>
                        <li><a href="#tab3">Por quê?</a></li>
                    </ul>
                    
                    <div id="tab1">
                        <div class="content_aba">
                            <!-- <div class="col4">
                                <div class="video">
                                    <a href="javascript;"><img src="../images/play.png" class="play"></a>
                                </div>
                                <p>Curabitur volutpat tempus velit a auctor. Morbi dignissim enim nulla, ut vestibu.</p>
                            </div> -->
                            <div class="col12 last">
                                <h4>O que é o Conecta?</h4>
                                <p>Eficiência Operacional e Segurança para o Paciente. Essas são as premissas do projeto CONECTA. O Philips Tasy foi escolhido, em um processo de concorrência, como o sistema de gestão hospitalar e prontuário eletrônico que está sendo implantado na Beneficência Portuguesa de São Paulo.</p>
                                <p>Utilizado e bem-sucedido em mais de 650 estabelecimentos no Brasil e América Central, o Philips Tasy é uma plataforma única e integrada, totalmente aderente à realidade dos mais modernos hospitais, clínicas de saúde, bancos de sangue, laboratórios, entre outros. A partir da integração promovida pelo Philips Tasy, o hospital tem a garantia da confiabilidade das informações que, cadastradas uma única vez no sistema, evitam o retrabalho e garantem a segurança, subsidiando a tomada de decisão em todos os níveis gerenciais.</p>
                            </div>
                            <!-- <hr>
                            <div class="col12">
                                <h4>Benefícios</h4>
                                <p>Diante de um cenário de constantes inovações, a tecnologia ultrapassou o simples processamento de dados padrão para áreas administrativas, comuns em todas as organizações, e agora desempenha um papel fundamental na área da saúde, tanto no cuidado ao paciente, na interpretação de um exame, como na prescrição médica, nas escalas de trabalho, nos relatórios de resultados e sistemas de prevenção.</p>
                                <p>Olhando nosso cenário e acompanhando tendências, a Beneficência Portuguesa de São Paulo iniciou, em 2014, o Projeto CONECTA, que vem conduzindo a implantação do mais moderno e eficiente sistema de gestão hospitalar do mercado brasileiro, o Philips Tasy.</p>
                                <p>Muito além de uma nova ferramenta de TI, o sistema traz uma mudança cultural, com benefícios diversos, como maior possibilidade de controle dos processos, maior segurança, atualização tecnológica, otimização do trabalho, retorno de investimento e acesso a informações de qualidade em tempo real para tomada de decisão, que trarão impactos positivos para colaboradores, médicos e, especialmente, para o paciente.</p>
                            </div> -->
                        </div>
                        <!-- <div class="clear"></div>
                        <div class="lista">
                            <ul class="list">
                                <li><strong>Aumento da confiança na informação recebida</strong></li>
                                <li><strong>Melhoria do fluxo de informações</strong></li>
                                <li><strong>Integração entre departamentos e pessoas</strong></li>
                                <li><strong>Automatização de processos e rotinas</strong></li>
                            </ul>
                            <ul class="list">
                                <li><strong>Diminuição do uso de papel</strong></li>
                                <li><strong>Utilização de protocolos internacionais para aumento da assertividade clínica</strong></li>
                                <li><strong>Conquista de certificados de qualidade</strong></li>
                            </ul>
                            <div class="clear"></div>
                        </div> -->
                    </div>

                    <div id="tab2">
                        <div class="content_aba pb0">
                            <h4>Como será realizado?</h4>
                            <p>A estratégia de execução estabelecida pela BP foi baseada em estudos que resultaram numa implementação por fases, com duração de dois anos, considerando as necessidades e desafios de cada unidade. As fases 1 (Corporativo e Hospital São José) e 2 (Hospital Santo Antônio) já foram realizadas e as experiências vividas nos dois hospitais estão sendo utilizadas como melhores práticas para as fases 3 e 4 (Hospital São Joaquim).</p>
                            <hr>
                            <h4>Quando será implementado?</h4>
                            <div class="image">
                                <div id="ex1" style="display:none;">
                                    <img src="../images/planejamento-macro.jpg">
                                </div>
                                <a href="#ex1" rel="modal:open"><img src="../images/planejamento-macro.jpg"></a>
                            </div>
                        </div>
                    </div>

                    <div id="tab3">
                        <div class="content_aba">
                            <h4>Por quê estamos realizando esta mudança de sistema?</h4>
                            <p>A implementação deste novo sistema traz muitos benefícios, como a integração, controle e a otimização de processos, segurança, rastreabilidade e integração das informações, além da melhoria nos resultados financeiros e, principalmente, a qualidade e segurança clínico assistencial, além, é claro, da melhoria no atendimento ao paciente, com a utilização da prescrição e prontuário eletrônicos.</p>
                            <div class="col12">
                                <h3 class="title_brasil">Informação que muda o Brasil</h3>
                                <div class="boxes">
                                    <div class="col2 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/brasil.jpg">
                                        </div>
                                        <div class="content_box">
                                            <p class="brasil">área de</p>
                                            <h2 class="brasil" style="font-size:38px;">8.515.767</h2>
                                            <p class="brasil"><span>km²</span></p>
                                        </div>
                                    </div>
                                    <div class="col2 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/populacao.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil" style="font-size:30px;margin-top:20px;">202.506.930</h2>
                                            <p class="brasil"><span>pessoas</span></p>
                                            <p class="brasil"></p>
                                        </div>
                                    </div>
                                    <div class="col2 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/internet.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil" style="margin-top:-15px;">95</h2>
                                            <p class="brasil"><span>milhões</span></p>
                                            <p class="brasil">de pessoas conectadas à internet</p>
                                        </div>
                                    </div>
                                    <div class="col2 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/mobile.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil">264</h2>
                                            <p class="brasil"><span>milhões</span></p>
                                            <p class="brasil">celulares</p>
                                        </div>
                                    </div>
                                    <div class="col2 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/hospital.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil">6.690</h2>
                                            <p class="brasil"><span>hospitais</span></p>
                                            <p class="brasil"></p>
                                        </div>
                                    </div>
                                    <div class="col2 box boxbrasil last">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/prontuario.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil">70%</h2>
                                            <p class="brasil"><span>com ERP</span></p>
                                            <p class="brasil">e prontuário</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col12">
                                    <h3 class="title_brasil mt30">Temos um mundo novo a nossa frente...<br>Um mundo cada vez mais rápido e conectado!</h3>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="mapa">
                                <img src="../images/mapa.jpg">
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>
                </div>
            </section>
        </section>

        <!-- <section id="depoimentos">
            <section class="central">
                <h2 class="title title-laranja">Depoimentos</h2>
                <div id="owl-demo3" class="owl-carousel">
                    <div class="item">
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video1">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor1.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Fernando Lorem</h4>
                                        <p>28 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video2">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Nam vel nisi scelerisque, lacinia tellus quis, cursus eros. Duis vel eleifend turpis, non scelerisque nunc. Integer ac facilisis augue.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor2.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutora Laura Ipsum</h4>
                                        <p>29 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4 last">
                            <div class="depoimento">
                                <div class="video3">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Uspendisse nec massa quis metus lacinia laoreet. Nam eget finibus nunc, in pretium nunc. Cras elementum purus nisl, eget.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor3.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Renan Amet</h4>
                                        <p>30 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video1">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor1.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Fernando Lorem</h4>
                                        <p>28 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video2">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Nam vel nisi scelerisque, lacinia tellus quis, cursus eros. Duis vel eleifend turpis, non scelerisque nunc. Integer ac facilisis augue.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor2.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutora Laura Ipsum</h4>
                                        <p>29 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4 last">
                            <div class="depoimento">
                                <div class="video3">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Uspendisse nec massa quis metus lacinia laoreet. Nam eget finibus nunc, in pretium nunc. Cras elementum purus nisl, eget.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor3.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Renan Amet</h4>
                                        <p>30 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video1">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor1.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Fernando Lorem</h4>
                                        <p>28 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video2">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Nam vel nisi scelerisque, lacinia tellus quis, cursus eros. Duis vel eleifend turpis, non scelerisque nunc. Integer ac facilisis augue.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor2.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutora Laura Ipsum</h4>
                                        <p>29 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4 last">
                            <div class="depoimento">
                                <div class="video3">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Uspendisse nec massa quis metus lacinia laoreet. Nam eget finibus nunc, in pretium nunc. Cras elementum purus nisl, eget.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor3.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Renan Amet</h4>
                                        <p>30 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section> -->

        <section id="treinamometro">
            <section class="central">
                <h3>Treinamômetro</h3>
                <p>Indicadores de Treinamento do Projeto Conecta</p>
                <p>Nesta página, você encontra os dados do Treinamômetro. Mas o que é isso?</p>
                <p>Na fase de implantação do TASY, no Hospital São Joaquim, você verá a palavra Treinamômetro muitas vezes. O Treinamômetro traz os indicadores de Treinamento do Projeto Conecta, separado por grupo – multiplicadores e usuários.</p>
                <p>Estes indicadores são atualizados duas vezes por semana e demonstram a quantidade de colaboradores envolvidos na implantação do TASY.</p>
                <a href="../Treinamometro_26_09.pdf" target="_blank">Clique aqui e veja os números</a>
            </section>
        </section>

        <section id="faq">
            <section class="central">
                <div class="col7 duvidas">
                    <h2 class="title title-verde">FAQ</h2>
                    <div id="owl-demo" class="owl-carousel">
                        <!-- linha1 -->
                        <div class="item">
                            <dl>
                                <dt class="bt_click ativo" rel="#duvida01">A implantação do Prontuário Eletrônico no Hospital São Joaquim será realizada com a certificação digital, ou seja, deixaremos de imprimir os prontuários?</dt>
                                <dd id="duvida01" class="toggleDiv" style="display:block">R: Não. A certificação digital está prevista apenas após o término da fase 4, ou seja, no segundo semestre.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida02">Customizações serão realizadas para que o Tasy possa se comportar da mesma maneira que o SGH, evitando assim alterações nos processos?</dt>
                                <dd id="duvida02" class="toggleDiv">R: Não. Os processos devem ser adaptados às melhores práticas do mercado, caso o Tasy realmente não atenda uma funcionalidade imprescindível, o Comitê do Projeto poderá autorizar a customização.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida03">Teremos controle de estoque e rastreabilidade dos OPME?</dt>
                                <dd id="duvida03" class="toggleDiv">R: Sim, o Tasy possibilitará, inclusive, o controle dos consignados.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida04">Adaptar os processos da minha área ao Tasy exigirá esforço e envolvimento de todos os meus colaboradores?</dt>
                                <dd id="duvida04" class="toggleDiv">R: Sim. O gestor identificará as necessidades e as estratégias de mudança de processo, mas todos deverão colaborar na implementação.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click ativo" rel="#duvida05">Conseguirei exportar relatórios para o Excel?</dt>
                                <dd id="duvida05" class="toggleDiv">R: Sim. A exportação para o Excel é uma funcionalidade presente na maioria dos relatórios gerados pelo Tasy.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida06">O Tasy substituirá todos os nossos sistemas, incluindo o sistema de RH do fabricante Totvs?</dt>
                                <dd id="duvida06" class="toggleDiv">R: Não, o RM não será substituído.</dd>
                            </dl>
                        </div>
                        <!-- fim linha1 -->
                        <!-- linha2 -->
                        <div class="item">
                            <dl>
                                <dt class="bt_click" rel="#duvida07">O Tasy será o "Salvador da Pátria" - todos os nossos problemas de processos serão resolvidos?</dt>
                                <dd id="duvida07" class="toggleDiv">R: Não. Como sistema, o Tasy pode facilitar a mudança de processos trazendo resultados promissores. Porém, periodicamente, os processos internos precisam ser analisados e revistos pelas equipes responsáveis.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida08">Serão realizados simulados pré-virada que testarão o nível de treinamento e integração dos processos?</dt>
                                <dd id="duvida08" class="toggleDiv">R: Sim. Testes pilotos estão planejados em cronograma.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida09">Todos os cadastros do SGH serão importados, ganhando tempo e velocidade na implementação?</dt>
                                <dd id="duvida09" class="toggleDiv">R: Não, muitos cadastros devem ser revistos para que a base seja íntegra.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida10">O Tasy poderá ser acessado pela internet?</dt>
                                <dd id="duvida10" class="toggleDiv">R: Sim, ele está preparado para acesso em ambientes externos à BP.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida11">Enfermagem bem treinada, segura e favorável ao sistema, facilita a utilização do Tasy pela equipe médica?</dt>
                                <dd id="duvida11" class="toggleDiv">R: Sim, sem dúvida. Uma equipe de enfermagem engajada e conhecedora do novo sistema facilita a mudança de processos.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida12">Fiz benchmarking com outro hospital que utiliza o Tasy. Na BP funcionará igual?</dt>
                                <dd id="duvida12" class="toggleDiv">R: A implementação do Tasy seguirá a estratégia de negócios da BP e será feita de acordo com as parametrizações descritas no projeto, podendo ser diferente do hospital visitado.</dd>
                            </dl>
                        </div>
                        <div class="item">
                            <dl>
                                <dt class="bt_click" rel="#duvida13">Os associados serão diretamente impactados nesta fase do projeto?</dt>
                                <dd id="duvida13" class="toggleDiv">R: Sim. A fase 3 prevê a “virada” de todas as recepções, incluindo a recepção do SAS – Serviço ao Associado, no HSJQ.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida14">O Tasy possui as escalas e índices baseados em referências bibliográficas?</dt>
                                <dd id="duvida14" class="toggleDiv">R: Sim. As escalas e índices são baseados em referências.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida15">Em uma nova contratação é desejável que o colaborador já tenha trabalhado com o Tasy?</dt>
                                <dd id="duvida15" class="toggleDiv">R: Sim, é desejável experiência com o sistema, mas este deverá ser treinado conforme o modelo implementado na BP.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida16">O CONECTA é um projeto somente de tecnologia?</dt>
                                <dd id="duvida16" class="toggleDiv">R: Não. O CONECTA é um projeto de toda a BP que trará benefícios em um âmbito geral.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida17">O gestor terá apoio do grupo de GM e RH para elaborar as escalas de treinamento do seu pessoal?</dt>
                                <dd id="duvida17" class="toggleDiv">R: Sim. Faz parte do escopo das atividades de GM e RH apoiar os gestores na elaboração das escalas de treinamento dos usuários finais.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida18">Os perfis de acesso serão revistos para a implantação do Tasy?</dt>
                                <dd id="duvida18" class="toggleDiv">R: Os grupos do CONECTA são responsáveis pela validação dos perfis de acesso junto aos gestores.</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col1 some">
                    <div class="line_div"></div>
                </div>
                <div class="col4 last pergunta">
                    <h3>Mande sua pergunta</h3>
                    <p>Donec interdum, neque sed sodales mattis, ex sem pellentesque sem.</p>
                    <form id="ajax_form">
                        <fieldset>
                            <input type="text" name="nome" placeholder="Nome">
                            <input type="email" name="email" placeholder="E-mail">
                            <textarea name="pergunta" placeholder="Sua pergunta"></textarea>
                            <input type="hidden" name="grava" value="grava">
                            <button type="submit">Enviar</button>
                        </fieldset>
                    </form>
                    <script type="text/javascript">
                        jQuery(document).ready(function(){
                            jQuery('#ajax_form').submit(function(){
                                var dados = jQuery( this ).serialize();
                                jQuery.ajax({
                                    type: "POST",
                                    url: "envia.php",
                                    data: dados,
                                    success: function( data )
                                    {
                                        alert(data);
                                    }
                                    error: function(){
                                        alert('Falha ao enviar!');
                                    }
                                });
                                return false;
                            });
                        });
                    </script>
                    <img src="../images/mac-faq.png" class="img-faq">
                </div>
            </section>
        </section>

        <!-- <section id="guias">
            <section class="central">
                <h2 class="title title-rosa">Guias</h2>
                <div id="owl-demo2" class="owl-carousel">
                    <div class="item">
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3 last">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3 last">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section> -->

        <footer>
            <section class="line height04"></section>
            <section class="central">
                <p class="copy"><img src="../images/logo-conecta.png"> <span>© 2016 - Todos os direitos reservados<span></p>
                <!-- <a id="#topo" href="javascript:;">topo</a> -->
            </section>
        </footer>

        <script src="../js/plugin/jquery.min.js"></script>
        <script src="../js/plugin/jquery-ui.min.js"></script>
        <script src="../js/tabs.js"></script>
        <script src="../js/faq.js"></script>
        <script>
            $("#datepicker").datepicker({
                inline: true,
                showOtherMonths: true,
                selectOtherMonths: true
            });
        </script>

        <script src="../owl-carousel/owl.carousel.js"></script>
        <script>
        $(document).ready(function() {
          var owl = $("#owl-demo");
          owl.owlCarousel({
            navigation : true
          });
          var owl = $("#owl-demo2");
          owl.owlCarousel({
            navigation : true
          });
          var owl = $("#owl-demo3");
          owl.owlCarousel({
            navigation : true
          });
        });
        </script>

        <script type="text/javascript">
            $("#irabas").click(function() {
                $('html, body').animate({
                    scrollTop: $("#abas").offset().top
                }, 1000);
            });
        </script>

    </body>

</html>