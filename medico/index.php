<!DOCTYPE HTML>
<html lang="pt-BR">

    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Conecta | Beneficência Portuguesa de São Paulo</title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="Marina Tieko Takahasi | Agência Mantra" />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" href="../css/swag.css" />
        <link rel="stylesheet" href="../css/home.css" />
        <link rel="stylesheet" href="../css/jquery-ui.css" />
        <link rel="stylesheet" href="../css/mobile.css" />
        <link rel="stylesheet" href="../css/font-awesome.css" />

        <!--[if lt IE 9]><script src="../http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <link href="../owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="../owl-carousel/owl.theme.css" rel="stylesheet">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

        <script src="../js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="../css/jquery.modal.css" type="text/css" media="screen" />

    </head>

    <body>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-34928349-7', 'auto');
    ga('send', 'pageview');

    </script>
        
        <header id="header">
            <section class="line height04"></section>
            <section class="central">
                <div class="row">
                    <div class="col3">
                        <h1 class="logo-conecta"><img src="../images/logo-conecta.png" width="115" alt="Conecta" title="Conecta"></h1>
                    </div>
                    <div class="col6 titulo-topo">
                        <h2>AGENDE SEU TREINAMENTO NO TASY</h2>
                    </div>
                    <div class="col3 last logo-bp">
                        <img src="../images/logo-bp.png" width="151" alt="Beneficência Portuguesa de São Paulo" title="Beneficência Portuguesa de São Paulo">
                    </div>
                </div>
            </section>
            <section class="line height01"></section>
        </header>

        <section id="banner">
            <section class="central">
                <h3>O que é o Philips Tasy?</h3>
                <p>O Philips Tasy é um sistema de gestão hospitalar que proporcionará mais segurança<br>aos pacientes, melhoria da eficiência operacional e diversos outros benefícios à instituição<br>e seus médicos, pacientes e colaboradores.</p>
                <a id="irabas" href="javascript:;">Saiba mais</a>
            </section>
        </section>

        <!-- <section id="agendamento">
            <section class="central">
                <div class="col12 steps">
                    <h4 class="data"><span class="active">1</span> data</h4>
                    <h4 class="horario"><span>2</span> horário</h4>
                    <h4 class="dados"><span>3</span> dados</h4>
                </div>
                <div class="col4 col-active">
                    <div id="datepicker"></div>
                </div>
                <div class="col4 horarios">
                    <dl>
                        <dt>23 de agosto</dt>
                        <dd>
                            <ul>
                                <li class="disable">09:00</li>
                                <li class="disable">10:00</li>
                                <li class="disable">11:00</li>
                                <li class="disable">12:00</li>
                                <li class="disable">13:00</li>
                                <li class="disable">14:00</li>
                                <li class="disable">15:00</li>
                                <li class="disable">16:00</li>
                                <li class="disable">17:00</li>
                                <li class="disable">18:00</li>
                                <li class="disable">19:00</li>
                                <li class="disable">20:00</li>
                            </ul>
                        </dd>
                    </dl>
                </div>
                <div class="col4 last dados">
                    <fieldset>
                        <div ng-app>
                            <input type="text" ng-model="nome" placeholder="Nome">
                            <input type="text" ng-model="crm" placeholder="CRM">
                            <input type="text" placeholder="E-mail">
                            <div class="termos">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vehicula lobortis hendrerit. Quisque nulla enim, mollis sed ornare a, facilisis tempor arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam rutrum mi et lorem auctor vestibulum. Etiam pretium vulputate maximus. Morbi mauris nibh, finibus at luctus et, eleifend sit amet eros. Donec suscipit molestie libero vitae condimentum. </div>
                            <p class="text">Eu "<span>{{ nome }}</span>" portador do CRM "<span>{{ crm }}</span>" lorem ipsum dolor sit amet.</p>
                            
                            <div class="squaredOne">
                                <input type="checkbox" value="None" id="squaredOne" name="check" />
                                <label for="squaredOne"></label>
                            </div>
                            <label for="aceito">Li e aceito</label>

                            <button>Enviar</button>
                        </div>
                    </fieldset>
                </div>
            </section>
        </section> -->

        <section id="abas">
            <section class="central">
                <div class="content">
                    <ul class="tabs" style="width:101%;">
                        <li><a href="#tab1" class="active">O que é?</a></li>
                        <li><a href="#tab2">Como?</a></li>
                        <li><a href="#tab3">Por quê?</a></li>
                    </ul>
                    
                    <div id="tab1">
                        <div class="content_aba">
                            <!-- <div class="col4">
                                <div class="video">
                                    <a href="javascript;"><img src="../images/play.png" class="play"></a>
                                </div>
                                <p>Curabitur volutpat tempus velit a auctor. Morbi dignissim enim nulla, ut vestibu.</p>
                            </div> -->
                            <div class="col12 last">
                                <h4>O que é o Philips Tasy?</h4>
                                <p>O Philips Tasy é um sistema de gestão hospitalar que proporcionará mais segurança aos pacientes, melhoria da eficiência operacional e diversos outros benefícios à instituição e seus médicos, pacientes e colaboradores.</p>
                                <p>A implantação será feita em etapas, sendo que a primeira delas, que está prevista para o início de dezembro 2016, facilitará a comunicação entre o hospital e as clínicas. Trata-se de uma plataforma única e integrada, totalmente aderente à realidade dos mais modernos hospitais, clínicas de saúde, consultórios, laboratórios e outros.O sistema já foi implantado nos hospitais São José e Santo Antônio com sucesso.</p>
                            </div>
                            <hr>
                            <div class="col12">
                                <h4>Benefícios</h4>
                                <p>Diante de um cenário de constantes inovações, a tecnologia ultrapassou o simples processamento de dados padrão para áreas administrativas, comuns em todas as organizações, e agora desempenha um papel fundamental na área da saúde, tanto no cuidado ao paciente, na interpretação de um exame, como na prescrição médica, nas escalas de trabalho, nos relatórios de resultados e sistemas de prevenção.</p>
                                <p>Olhando nosso cenário e acompanhando tendências, a Beneficência Portuguesa de São Paulo iniciou, em 2014, o Projeto CONECTA, que vem conduzindo a implantação do mais moderno e eficiente sistema de gestão hospitalar do mercado brasileiro, o Philips Tasy.</p>
                                <p>Muito além de uma nova ferramenta de TI, o sistema traz uma mudança cultural, com benefícios diversos, como maior possibilidade de controle dos processos, maior segurança, atualização tecnológica, otimização do trabalho, retorno de investimento e acesso a informações de qualidade em tempo real para tomada de decisão, que trarão impactos positivos para colaboradores, médicos e, especialmente, para o paciente.</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="lista">
                            <ul class="list">
                                <li><strong>Aumento da confiança na informação recebida</strong></li>
                                <li><strong>Melhoria do fluxo de informações</strong></li>
                                <li><strong>Integração entre departamentos e pessoas</strong></li>
                                <li><strong>Automatização de processos e rotinas</strong></li>
                            </ul>
                            <ul class="list">
                                <li><strong>Diminuição do uso de papel</strong></li>
                                <li><strong>Utilização de protocolos internacionais para aumento da assertividade clínica</strong></li>
                                <li><strong>Conquista de certificados de qualidade</strong></li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div id="tab2">
                        <div class="content_aba pb0">
                            <h4>Como eu serei impactado?</h4>
                            <p>A 1ª etapa desta fase, prevista para início de dezembro de 2016, implantará o sistema em todas as recepções dos consultórios, e facilitará a comunicação entre o hospital e as clínicas.</p>
                            <p>Nesta etapa, o novo sistema permitirá ao médico realizar:</p>
                        </div>
                        <div class="clear"></div>
                        <div class="lista">
                            <ul class="list">
                                <li><strong>Agendamento de consultas</strong></li>
                                <li><strong>Agendamento de exames</strong></li>
                                <li><strong>Controle dos honorários médicos</strong></li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div class="content_aba">
                            <p>Vale ressaltar que a infraestrutura tecnológica (computadores e redes) para as recepções dos consultórios, será disponibilizada pela instituição.</p>
                            <p>Com toda essa mudança, o conhecimento sobre a ferramenta é fundamental, por isso a BP oferecerá treinamento às secretárias/assistentes a partir do mês de setembro.</p>
                            <hr>
                            <h4>Quando serei impactado?</h4>
                            <div class="image">
                                <div id="ex1" style="display:none;">
                                    <img src="../images/planejamento-macro.jpg">
                                </div>
                                <a href="#ex1" rel="modal:open"><img src="../images/planejamento-macro.jpg"></a>
                            </div>
                        </div>
                    </div>

                    <div id="tab3">
                        <div class="content_aba">
                            <h4>Por quê estamos realizando esta mudança de sistema?</h4>
                            <div class="col6 firstcol">
                                <h3 class="title_mundo">Informação que muda o mundo</h3>
                                <div class="boxes">
                                    <div class="col4 box boxmundo">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/conectados.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">2,7</h2>
                                            <p class="mundo"><span>bilhões</span></p>
                                            <p class="mundo">pessoas online</p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/casa.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">750</h2>
                                            <p class="mundo"><span>milhões</span></p>
                                            <p class="mundo">de casas conectadas</p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo last">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/mobile.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">7,3</h2>
                                            <p class="mundo"><span>bilhões</span></p>
                                            <p class="mundo">celulares no mundo</p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/facebook.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">1,23</h2>
                                            <p class="mundo"><span>bilhões</span></p>
                                            <p class="mundo"></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/linkedin.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">300</h2>
                                            <p class="mundo"><span>milhões</span></p>
                                            <p class="mundo"></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo last">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/youtube.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">4</h2>
                                            <p class="mundo"><span>bilhões</span></p>
                                            <p class="mundo"></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/arroba.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">3</h2>
                                            <p class="mundo"><span>bilhões</span></p>
                                            <p class="mundo"></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/whatsapp.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">600</h2>
                                            <p class="mundo"><span>milhões</span></p>
                                            <p class="mundo"></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxmundo last">
                                        <div class="icone">
                                            <img src="../images/icones/mundo/instagram.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="mundo">200</h2>
                                            <p class="mundo"><span>milhões</span></p>
                                            <p class="mundo"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col6 last">
                                <h3 class="title_brasil">Informação que muda o Brasil</h3>
                                <div class="boxes">
                                    <div class="col4 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/brasil.jpg">
                                        </div>
                                        <div class="content_box">
                                            <p class="brasil">área de</p>
                                            <h2 class="brasil" style="font-size:38px;">8.515.767</h2>
                                            <p class="brasil"><span>km²</span></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/populacao.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil" style="font-size:30px;margin-top:20px;">202.506.930</h2>
                                            <p class="brasil"><span>pessoas</span></p>
                                            <p class="brasil"></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxbrasil last">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/internet.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil" style="margin-top:-15px;">95</h2>
                                            <p class="brasil"><span>milhões</span></p>
                                            <p class="brasil">de pessoas conectadas à internet</p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/mobile.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil">264</h2>
                                            <p class="brasil"><span>milhões</span></p>
                                            <p class="brasil">celulares</p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxbrasil">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/hospital.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil">6.690</h2>
                                            <p class="brasil"><span>hospitais</span></p>
                                            <p class="brasil"></p>
                                        </div>
                                    </div>
                                    <div class="col4 box boxbrasil last">
                                        <div class="icone">
                                            <img src="../images/icones/brasil/prontuario.jpg">
                                        </div>
                                        <div class="content_box">
                                            <h2 class="brasil">70%</h2>
                                            <p class="brasil"><span>com ERP</span></p>
                                            <p class="brasil">e prontuário</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col12">
                                    <h3 class="title_brasil mt30">Temos um mundo novo a nossa frente...<br>Um mundo cada vez mais rápido e conectado!</h3>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="mapa">
                                <img src="../images/mapa.jpg">
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>
                </div>
            </section>
        </section>

        <!-- <section id="depoimentos">
            <section class="central">
                <h2 class="title title-laranja">Depoimentos</h2>
                <div id="owl-demo3" class="owl-carousel">
                    <div class="item">
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video1">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor1.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Fernando Lorem</h4>
                                        <p>28 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video2">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Nam vel nisi scelerisque, lacinia tellus quis, cursus eros. Duis vel eleifend turpis, non scelerisque nunc. Integer ac facilisis augue.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor2.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutora Laura Ipsum</h4>
                                        <p>29 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4 last">
                            <div class="depoimento">
                                <div class="video3">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Uspendisse nec massa quis metus lacinia laoreet. Nam eget finibus nunc, in pretium nunc. Cras elementum purus nisl, eget.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor3.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Renan Amet</h4>
                                        <p>30 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video1">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor1.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Fernando Lorem</h4>
                                        <p>28 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video2">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Nam vel nisi scelerisque, lacinia tellus quis, cursus eros. Duis vel eleifend turpis, non scelerisque nunc. Integer ac facilisis augue.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor2.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutora Laura Ipsum</h4>
                                        <p>29 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4 last">
                            <div class="depoimento">
                                <div class="video3">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Uspendisse nec massa quis metus lacinia laoreet. Nam eget finibus nunc, in pretium nunc. Cras elementum purus nisl, eget.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor3.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Renan Amet</h4>
                                        <p>30 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video1">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor1.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Fernando Lorem</h4>
                                        <p>28 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4">
                            <div class="depoimento">
                                <div class="video2">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Nam vel nisi scelerisque, lacinia tellus quis, cursus eros. Duis vel eleifend turpis, non scelerisque nunc. Integer ac facilisis augue.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor2.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutora Laura Ipsum</h4>
                                        <p>29 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col4 last">
                            <div class="depoimento">
                                <div class="video3">
                                    <a href=""><img src="../images/play.png" class="play"></a>
                                </div>
                                <div class="content_depoimento">
                                    <h3>Nulla varius euismod nisl at facilisis</h3>
                                    <p>Uspendisse nec massa quis metus lacinia laoreet. Nam eget finibus nunc, in pretium nunc. Cras elementum purus nisl, eget.</p>
                                </div>
                                <div class="autor">
                                    <div class="col2">
                                        <img src="../images/autor3.png">
                                    </div>
                                    <div class="col9">
                                        <h4>Doutor Renan Amet</h4>
                                        <p>30 Agosto, 2016</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section> -->

        <section id="faq">
            <section class="central">
                <div class="col7 duvidas">
                    <h2 class="title title-verde">FAQ</h2>
                    <div id="owl-demo" class="owl-carousel">
                        <!-- linha1 -->
                        <div class="item">
                            <dl>
                                <dt class="bt_click ativo" rel="#duvida01">Mesmo utilizando o sistema Philips Tasy em outros hospitais, devo participar dos treinamentos?</dt>
                                <dd id="duvida01" class="toggleDiv" style="display:block">R: Sim. Trata-se de uma versão diferente do sistema, dessa forma é imprescindível a realização do treinamento.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida02">Haverá profissionais para nos orientar na utilização do sistema Philips Tasy?</dt>
                                <dd id="duvida02" class="toggleDiv">R: Sim. A equipe de TI e os multiplicadores estarão à disposição durante o período de estabilização. Posteriormente será possível contar com os multiplicadores e com os profissionais da enfermagem.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida03">Como será feito o acesso do médico ao sistema?</dt>
                                <dd id="duvida03" class="toggleDiv">R: Todos os médicos deverão ser cadastrados e terão login único e intransferível para acesso ao Philips Tasy.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida04">O cálculo de HM (honorários médicos) será feito pelo sistema?</dt>
                                <dd id="duvida04" class="toggleDiv">R: Sim. Os honorários médicos serão calculados no sistema, sendo integrados aos dados de outras unidades hospitalares da BP.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click ativo" rel="#duvida05">O sistema Alert, utilizado no Pronto-Socorro, será substituído pelo Philips Tasy?</dt>
                                <dd id="duvida05" class="toggleDiv">R: Sim.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida06">Os anestesistas deverão utilizar o sistema para registrar os agentes anestésicos, medicamentos utilizados e evolução dos pacientes?</dt>
                                <dd id="duvida06" class="toggleDiv">R: Sim. Todas as especialidades utilizarão o Philips Tasy.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida07">Deixarei de imprimir os prontuários?</dt>
                                <dd id="duvida07" class="toggleDiv">R: Não. A certificação digital só será estudada posteriormente, portanto continuaremos imprimindo os prontuários.</dd>
                            </dl>
                        </div>
                        <!-- fim linha1 -->
                        <!-- linha2 -->
                        <div class="item">
                            <dl>
                                <dt class="bt_click" rel="#duvida08">Com a utilização da prescrição eletrônica, não serão necessárias a impressão física e a assinatura do médico?</dt>
                                <dd id="duvida08" class="toggleDiv">R: Sim, serão necessárias. Até que a certificação digital seja implantada, as prescrições deverão ser impressas e assinadas.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida09">Poderei solicitar exames laboratoriais e radiológicos pelo sistema Philips Tasy?</dt>
                                <dd id="duvida09" class="toggleDiv">R: Todos os pedidos médicos serão realizados via sistema. As prescrições estarão integradas com os sistemas de imagem e laboratório.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida10">Será possível visualizar os resultados de exames laboratoriais no sistema?</dt>
                                <dd id="duvida10" class="toggleDiv">R: Sim. Todos os exames poderão ser consultados no sistema.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida11">Poderei visualizar o prontuário do paciente à distância?</dt>
                                <dd id="duvida11" class="toggleDiv">R: Sim. Tecnicamente é possível visualizar o prontuário pela internet, no entanto esta condição deve ser alinhada à política de acesso ao prontuário da BP.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida12">Poderei acessar o prontuário eletrônico do paciente de forma integral e não apenas por unidade hospitalar?</dt>
                                <dd id="duvida12" class="toggleDiv">R: Sim. O prontuário eletrônico poderá ser acessado considerando todas as unidades hospitalares da instituição.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida13">É possível verificar os acessos ao prontuário eletrônico do paciente?</dt>
                                <dd id="duvida13" class="toggleDiv">R: Todas as consultas, inserções e alterações no prontuário eletrônico do paciente ficam registradas no sistema, possibilitando a rastreabilidade das informações.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida14">Quem será responsável por inserir as prescrições eletrônicas no sistema?</dt>
                                <dd id="duvida14" class="toggleDiv">R: As prescrições eletrônicas serão inseridas no sistema pelos próprios médicos.</dd>
                            </dl>
                        </div>
                        <div class="item">
                            <dl>
                                <dt class="bt_click" rel="#duvida15">Quais serão as opções de consulta de medicamentos padronizados?</dt>
                                <dd id="duvida15" class="toggleDiv">R: Será possível consultar pelo princípio ativo e pelo nome comercial, além da unidade hospitalar, facilitando assim a prescrição médica.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida16">Haverá informações sobre dosagem de medicamentos na prescrição eletrônica?</dt>
                                <dd id="duvida16" class="toggleDiv">R: Sim. O médico será informado quando houver uma prescrição de medicamento com a dosagem acima do preconizado em literatura.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida17">Interações medicamentosas estarão implantadas no Philips Tasy?</dt>
                                <dd id="duvida17" class="toggleDiv">R: Sim. O sistema vai alertar as possíveis interações classificadas como graves, auxiliando o médico na tomada de decisão.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida18">Como foram definidas as escalas e índices disponíveis no Philips Tasy?</dt>
                                <dd id="duvida18" class="toggleDiv">R: As escalas e índices disponíveis no sistema são baseados em referências bibliográficas oficiais.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida19">Será permitido programar a administração futura de medicamentos?</dt>
                                <dd id="duvida19" class="toggleDiv">R: Sim. O sistema está habilitado para que o médico agende a administração futura de medicamentos em caso de necessidade.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida20">O Philips Tasy permitirá a inserção de receitas padronizadas para agilizar a digitação?</dt>
                                <dd id="duvida20" class="toggleDiv">R: Sim, poderão ser inseridas, facilitando a digitação dos médicos.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida21">O Philips Tasy está configurado para nos alertar caso o paciente tenha alergia ao medicamento prescrito?</dt>
                                <dd id="duvida21" class="toggleDiv">R: Sim. Quando informado no histórico que o paciente é alérgico a algum medicamento, o médico será alertado no momento da elaboração da prescrição.</dd>
                            </dl>
                        </div>
                        <div class="item">
                            <dl>
                                <dt class="bt_click" rel="#duvida22">O sistema nos auxiliará na contagem dos dias de administração de um determinado medicamento?</dt>
                                <dd id="duvida22" class="toggleDiv">R: Sim.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida23">Com a utilização da prescrição eletrônica, será possível utilizar prescrições anteriores do paciente?</dt>
                                <dd id="duvida23" class="toggleDiv">R: Sim. Poderão ser utilizadas prescrições anteriores para continuidade do tratamento.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida24">Com a prescrição eletrônica, as justificativas sobre utilização de medicamentos não padronizados ou de antimicrobianos controlados pela SCIH serão fei24s no sistema?</dt>
                                <dd id="duvida24" class="toggleDiv">R: Sim.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida25">Poderei realizar a inclusão de consulta de medicamentos de uso domiciliar no sistema?</dt>
                                <dd id="duvida25" class="toggleDiv">R: Sim. O sistema prevê a possibilidade de inclusão de medicamentos de uso contínuo, facilitando a continuidade do cuidado.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida26">O sistema proporcionará agilidade na administração de um medicamento em caso de urgência?</dt>
                                <dd id="duvida26" class="toggleDiv">R: Sim. A prescrição médica de um item urgente cairá automaticamente na farmácia para separação e dispensação, não necessitando da intervenção humana para esta solicitação.</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida27">O sistema facilitará a identificação dos medicamentos com nomenclatura parecida?</dt>
                                <dd id="duvida27" class="toggleDiv">R: Sim. Na elaboração dos cadastros dos medicamentos com nomes parecidos serão utilizados tamanhos diferentes de letras de forma a diferenciar os nomes dos itens, prevenindo assim possíveis erros. Exemplo: CarBAMazepina X Oxcarbazepina</dd>
                            </dl>
                            <dl>
                                <dt class="bt_click" rel="#duvida28">Onde serão instalados computadores para utilização do sistema?</dt>
                                <dd id="duvida28" class="toggleDiv">R: Serão instalados computadores para utilização dos médicos, seguindo a métrica:<br>Unidades de internação:  1 beira-leito (carrinho de medicação com notebook) a cada 4 leitos + 1 computador a cada 5 leitos.<br>UTI:  1 beira-leito (carrinho de medicação com notebook) a cada 3 leitos + 1 computador a cada 4 leitos.</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col1 some">
                    <div class="line_div"></div>
                </div>
                <div class="col4 last pergunta">
                    <h3>Mande sua pergunta</h3>
                    <p>Donec interdum, neque sed sodales mattis, ex sem pellentesque sem.</p>
                    <form>
                        <fieldset>
                            <input type="" name="" placeholder="Nome">
                            <input type="" name="" placeholder="E-mail">
                            <textarea name="" placeholder="Sua pergunta"></textarea>
                            <button type="submit">Enviar</button>
                        </fieldset>
                    </form>
                    <img src="../images/mac-faq.png" class="img-faq">
                </div>
            </section>
        </section>

        <!-- <section id="guias">
            <section class="central">
                <h2 class="title title-rosa">Guias</h2>
                <div id="owl-demo2" class="owl-carousel">
                    <div class="item">
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3 last">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col3 last">
                            <div class="guia">
                                <div class="icone">
                                    <img src="../images/pdf.png">
                                </div>
                                <div class="conteudo">
                                    <h4>Nulla varius euismod nisl at facilisis</h4>
                                    <p>Lorem ipsum dolor sit amet, to consectetur  adipi scing elit. Nulla vel vehicula lorem and lacus. Vestibulum vitae mauris.</p>
                                    <a href="">Baixar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section> -->

        <footer>
            <section class="line height04"></section>
            <section class="central">
                <p class="copy"><img src="../images/logo-conecta.png"> <span>© 2016 - Todos os direitos reservados</span></p>
                <!-- <a id="#topo" href="javascript:;">topo</a> -->
            </section>
        </footer>

        <script src="../js/plugin/jquery.min.js"></script>
        <script src="../js/plugin/jquery-ui.min.js"></script>
        <script src="../js/tabs.js"></script>
        <script src="../js/faq.js"></script>
        <script>
            $("#datepicker").datepicker({
                inline: true,
                showOtherMonths: true,
                selectOtherMonths: true
            });
        </script>

        <script src="../owl-carousel/owl.carousel.js"></script>
        <script>
        $(document).ready(function() {
          var owl = $("#owl-demo");
          owl.owlCarousel({
            navigation : true
          });
          var owl = $("#owl-demo2");
          owl.owlCarousel({
            navigation : true
          });
          var owl = $("#owl-demo3");
          owl.owlCarousel({
            navigation : true
          });
        });
        </script>

        <script type="text/javascript">
            $("#irabas").click(function() {
                $('html, body').animate({
                    scrollTop: $("#abas").offset().top
                }, 1000);
            });
        </script>

    </body>

</html>