<header id="header">
            <section class="line height04"></section>
            <section class="central">
                <div class="row">
                    <div class="col3">
                        <h1 class="logo-conecta"><img src="<?php echo URL; ?>images/logo-conecta.png" width="115" alt="Conecta" title="Conecta"></h1>
                    </div>
                    <div class="col6 titulo-topo">
                        <h2>AGENDE SEU TREINAMENTO NO TASY</h2>
                    </div>
                    <div class="col3 last logo-bp">
                        <img src="<?php echo URL; ?>images/logo-bp.png" width="151" alt="Beneficência Portuguesa de São Paulo" title="Beneficência Portuguesa de São Paulo">
                    </div>
                </div>
            </section>
            <section class="line height01"></section>
        </header>