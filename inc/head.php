<?php include_once('config.php'); ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Conecta | Beneficência Portuguesa de São Paulo</title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="Marina Tieko Takahasi | Agência Mantra" />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" href="<?php echo URL; ?>css/swag.css" />
        <link rel="stylesheet" href="<?php echo URL; ?>css/home.css" />
        <link rel="stylesheet" href="<?php echo URL; ?>css/jquery-ui.css" />
        <link rel="stylesheet" href="<?php echo URL; ?>css/mobile.css" />
        <link rel="stylesheet" href="<?php echo URL; ?>css/font-awesome.css" />

        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <link href="<?php echo URL; ?>owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo URL; ?>owl-carousel/owl.theme.css" rel="stylesheet">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>