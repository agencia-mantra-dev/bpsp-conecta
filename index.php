<!DOCTYPE HTML>
<html lang="pt-BR">

    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Conecta | Beneficência Portuguesa de São Paulo</title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="Marina Tieko Takahasi | Agência Mantra" />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/intro.css" />
        <link rel="stylesheet" href="css/jquery-ui.css" />
        <link rel="stylesheet" href="css/mobile.css" />
        <link rel="stylesheet" href="css/font-awesome.css" />

        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <link href="owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="owl-carousel/owl.theme.css" rel="stylesheet">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>

    </head>

    <body>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-34928349-7', 'auto');
    ga('send', 'pageview');

    </script>
        
        <header id="header">
            <section class="line height04"></section>
            <section class="central">
                <div class="row">
                    <div class="col3">
                        <h1 class="logo-conecta"><img src="images/logo-conecta.png" width="115" alt="Conecta" title="Conecta"></h1>
                    </div>
                    <div class="col6 titulo-topo">
                        <h2>AGENDE SEU TREINAMENTO NO TASY</h2>
                    </div>
                    <div class="col3 last logo-bp">
                        <img src="images/logo-bp.png" width="151" alt="Beneficência Portuguesa de São Paulo" title="Beneficência Portuguesa de São Paulo">
                    </div>
                </div>
            </section>
            <section class="line height01"></section>
        </header>

        <section id="intro">
            <div class="box-intro">
                <div class="topo">
                    <h1>Quem é você?</h1>
                </div>
                <div class="line-intro"></div>
                <div class="boxes">
                    <div class="icone">
                        <img src="images/i-medico.png">
                    </div>
                    <a href="medico">Médico</a><br>
                    <small>ou secretária</small>
                </div>
                <div class="boxes">
                    <div class="icone">
                        <img src="images/i-colaborador.png">
                    </div>
                    <a href="colaborador">Colaborador</a>
                </div>
                <div class="clear"></div>
            </div>
        </section>

        <footer>
            <section class="line height04"></section>
            <section class="central">
                <p class="copy"><img src="images/logo-conecta.png"> © 2016 - Todos os direitos reservados</p>
            </section>
        </footer>

    </body>
</html>